﻿namespace Core
{
    public interface IKeyGenerator
    {
        string LikeGuid();
        string YearMonth(int year, int month);
    }
}