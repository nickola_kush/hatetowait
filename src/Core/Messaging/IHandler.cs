﻿using System.Threading.Tasks;

namespace Core.Messaging
{
    public interface IHandler<in T> where T : IMessage
    {
        Task Handle(T message);
    }
}