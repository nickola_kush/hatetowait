﻿using System;

namespace Core.Messaging
{
    public interface IHandlersProvider
    {
        object[] GetHandlersFor(Type messageType);
    }
}