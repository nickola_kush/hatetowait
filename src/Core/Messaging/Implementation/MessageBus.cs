﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Messaging.Implementation
{
    public class MessageBus : IMessageBus
    {
        private readonly IHandlersProvider provider;

        public MessageBus(IHandlersProvider provider)
        {
            this.provider = provider;
        }

        public async Task Publish<T>(IEnumerable<T> subject) where T : IMessage
        {
            foreach (var message in subject)
            {
                await Publish(message);
            }
        }

        public async Task Publish<T>(T subject) where T : IMessage
        {
            var messageType = subject.GetType();
            var handlers = provider.GetHandlersFor(messageType);
            if (subject is ICommand && handlers.Length > 1)
            {
                throw new Exception($"Multiple handlers found, for command {messageType.FullName}");
            }

            foreach (var handler in handlers)
            {
                await ((dynamic)handler).Handle((dynamic)subject);
            }
        }
    }
}
