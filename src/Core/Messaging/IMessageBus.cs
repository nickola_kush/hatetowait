﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Messaging
{
    public interface IMessageBus
    {
        Task Publish<T>(IEnumerable<T> subject) where T : IMessage;
        Task Publish<T>(T subject) where T : IMessage;
    }
}
