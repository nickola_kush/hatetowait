﻿using System;

namespace Core.Implementation
{
    public class KeyGenerator : IKeyGenerator
    {
        public string LikeGuid()
        {
            return Guid.NewGuid().ToString("N");
        }

        public string YearMonth(int year, int month)
        {
            return $"{year} - {month}";
        }
    }
}