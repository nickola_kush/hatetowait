using System.Reflection;

[assembly: AssemblyProduct("H82W8")]

[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("Copyright © Mykola Kush 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("0.0.1.0")]
//Change MAJOR version before release
//Change MINOR version when you add functionality in a backwards-compatible manner
//Change PATCH version when you make backwards-compatible bugfixes