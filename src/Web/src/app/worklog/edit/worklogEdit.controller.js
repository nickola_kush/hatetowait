﻿export default class WorklogEditController {
    constructor($stateParams, worklogService) {
        worklogService.get($stateParams.id).then(res => {
            this.model = res.data;
        });
    }
}