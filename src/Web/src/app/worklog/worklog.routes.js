﻿routes.$inject = ['$stateProvider', 'State'];

export default function routes($stateProvider, State) {
    $stateProvider
        .state(State.Worklog, {
            url: '/worklog',
            views: {
                'main@': {
                    template: require('./worklog.html'),
                    controller: 'WorklogController',
                    controllerAs: 'ctrl'
                }
            }
        })
        .state(State.WorklogCreate, {
            url: '/create',
            views: {
                'main@': {
                    template: require('./create/create.html'),
                    controller: 'WorklogCreateController',
                    controllerAs: 'ctrl'
                }
            }
        })
        .state(State.WorklogEdit, {
            url: '/{id}/edit',
            views: {
                'main@': {
                    template: require('./edit/edit.html'),
                    controller: 'WorklogEditController',
                    controllerAs: 'ctrl'
                }
            }
        });
}