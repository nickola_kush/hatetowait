﻿import angular from 'angular';
import uirouter from 'angular-ui-router';
import state from '../shared/constants/state';

import routing from './worklog.routes';
import worklogController from './worklog.controller';
import worklogCreateController from './create/worklogCreate.controller';
import worklogEditController from './edit/worklogEdit.controller';

export default angular.module('h82w8.worklog', [uirouter, state])
  .config(routing)
  .controller('WorklogController', worklogController)
  .controller('WorklogCreateController', worklogCreateController)
  .controller('WorklogEditController', worklogEditController)
  .name;