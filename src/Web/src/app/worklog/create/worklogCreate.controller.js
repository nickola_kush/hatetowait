﻿export default class WorklogCreateController {
    constructor($state, State, worklogService) {
        this.$state = $state;
        this.State = State;
        this.worklogService = worklogService;
    }

    submit() {
        this.worklogService.create(this.model).then((response) => {
            this.$state.go(this.State.WorklogEdit, { id: response.data });
        });
    }
}