﻿export default class WorklogController {
    constructor(worklogService) {
        worklogService.getAll().then((response)=> {
            this.worklogs = response.data;
        });
    }
}