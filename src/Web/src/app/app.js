import '../style/app.css';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import dashboard from './dashboard';
import worklog from './worklog';
import invoice from './invoice';
import state from './shared/constants/state';
import services from './shared/services';

const moduleName = 'h82w8';

angular.module(moduleName, [uirouter, dashboard, worklog, invoice, state, services])
    .config(['$urlRouterProvider', '$locationProvider', function($urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
    }]);

export default moduleName;