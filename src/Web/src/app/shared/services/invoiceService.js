﻿import angular from 'angular';

class Service {
    constructor($http) {
        this.$http = $http;
        this.baseUrl = '/api/invoice';
    }

    get(year, month) {
        return this.$http.get(this.baseUrl + '/get?year=' + year + '&month=' + month);
    }

    getAll() {
        return this.$http.get(this.baseUrl + '/getall');
    }
}

export default angular.module('services.invoice', [])
  .service('invoiceService', Service)
  .name;