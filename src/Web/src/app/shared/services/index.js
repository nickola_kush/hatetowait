﻿import invoiceService from './invoiceService';
import worklogService from './worklogService';

export default angular.module('services', [invoiceService, worklogService])
    .name;