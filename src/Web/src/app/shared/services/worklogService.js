﻿import angular from 'angular';

class Service {
    constructor($http) {
        this.$http = $http;
        this.baseUrl = '/api/worklog';
    }

    create(model) {
        return this.$http.put(this.baseUrl + '/create', model);
    }

    get(id) {
        return this.$http.get(this.baseUrl + '/get?id=' + id);
    }

    getAll() {
        return this.$http.get(this.baseUrl + '/getall');
    }
}

export default angular.module('services.worklog', [])
  .service('worklogService', Service)
  .name;