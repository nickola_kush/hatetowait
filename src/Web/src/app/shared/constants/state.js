﻿import angular from 'angular';

var Constant = Object.freeze({
    Worklog: 'worklog',
    WorklogCreate: 'worklog.create',
    WorklogEdit: 'worklog.edit',

    Invoice: 'invoice',
    InvoicePrint: 'invoice.print'
});

export default angular.module('constant.state', [])
  .constant('State', Constant)
  .name;