﻿export default class InvoicePrintController {
    constructor($stateParams, invoiceService) {
        invoiceService.get($stateParams.year, $stateParams.month).then(res => {
            this.model = res.data;
        });
    }
}