﻿import angular from 'angular';
import uirouter from 'angular-ui-router';
import state from '../shared/constants/state';

import routing from './invoice.routes';
import invoiceController from './invoice.controller';
import invoicePrintController from './print/invoicePrint.controller';

export default angular.module('h82w8.invoice', [uirouter, state])
  .config(routing)
  .controller('InvoiceController', invoiceController)
  .controller('InvoicePrintController', invoicePrintController)
  .name;