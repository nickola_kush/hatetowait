﻿routes.$inject = ['$stateProvider', 'State'];

export default function routes($stateProvider, State) {
    $stateProvider
        .state(State.Invoice, {
            url: '/invoice',
            views: {
                'main@': {
                    template: require('./invoice.html'),
                    controller: 'InvoiceController',
                    controllerAs: 'ctrl'
                }
            }
        })
        .state(State.InvoicePrint, {
            url: '/{year}/{month}',
            views: {
                'main@': {
                    template: require('./print/print.html'),
                    controller: 'InvoicePrintController',
                    controllerAs: 'ctrl'
                }
            }     
        })
    ;
}