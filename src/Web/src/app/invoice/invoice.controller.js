﻿export default class InvoiceController {
    constructor(invoiceService) {
        invoiceService.getAll().then(res => {
            this.model = res.data;
        });
    }
}