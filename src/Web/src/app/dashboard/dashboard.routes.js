﻿routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
      .state('dashboard', {
          url: '/',
          views: {
              'main@': {
                  template: require('./dashboard.html'),
                  controller: 'DashboardController',
                  controllerAs: 'ctrl'
              }
          }
      });
}