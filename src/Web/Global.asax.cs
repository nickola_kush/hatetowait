﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using Core;
using Core.Implementation;
using Core.Messaging;
using Core.Messaging.Implementation;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Newtonsoft.Json;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using Web;
using Web.Core.Configurations;
using Web.Core.NinjectHelpers;
using Work;
using Work.ReadModel;
using Work.ReadModel.Implementation;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebApiApplication), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(WebApiApplication), "Stop")]
namespace Web
{
    public class WebApiApplication : HttpApplication
    {
        // ReSharper disable once InconsistentNaming
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(RegisterHttp);
        }

        public static void RegisterHttp(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };

        }

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                RegisterMessageBus(kernel);

                GlobalConfiguration.Configuration.DependencyResolver = new NinjectHttpDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IKeyGenerator>().To<KeyGenerator>().InSingletonScope();

            kernel.Bind<IWorkConfiguration>().ToMethod(c => WorkSection.Configuration).InSingletonScope();
            kernel.Bind<IWorklogDao>().To<WorklogDao>().InSingletonScope();
            kernel.Bind<IInvoiceDao>().To<InvoiceDao>().InSingletonScope();
        }

        private static void RegisterMessageBus(IBindingRoot kernel)
        {
            kernel.Bind<IHandlersProvider>().ToMethod(c => new HandlersProvider(c.Kernel)).InSingletonScope();
            kernel.Bind<IMessageBus>().To<MessageBus>().InSingletonScope();

            var allTypes = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => !a.IsDynamic)
                .SelectMany(a => a.GetExportedTypes())
                .ToArray();

            kernel.BindAllInterfaces(typeof(IHandler<>), allTypes);
        }
    }
}
