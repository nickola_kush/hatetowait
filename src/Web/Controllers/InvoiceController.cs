﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Work.Contract.Models;
using Work.ReadModel;

namespace Web.Controllers
{
    [RoutePrefix("api/invoice")]
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceDao invoice;

        public InvoiceController(IInvoiceDao invoice)
        {
            this.invoice = invoice;
        }

        [HttpGet, Route("get")]
        public Task<InvoiceViewModel> Get(int year, int month)
        {
            return invoice.GetByYearAndMonth(year, month);
        }

        [HttpGet, Route("getall")]
        public Task<ICollection<InvoiceViewModel>> GetAll()
        {
            return invoice.GetAll();
        }
    }
}