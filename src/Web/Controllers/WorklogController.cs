﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Core;
using Core.Messaging;
using Web.Models;
using Work.Contract.Commands;
using Work.Contract.Models;
using Work.ReadModel;

namespace Web.Controllers
{
    [RoutePrefix("api/worklog")]
    public class WorklogController : ApiController
    {
        private readonly IMessageBus messageBus;
        private readonly IKeyGenerator keyGenerator;
        private readonly IWorklogDao worklog;

        public WorklogController(IMessageBus messageBus, IKeyGenerator keyGenerator, IWorklogDao worklog)
        {
            this.messageBus = messageBus;
            this.keyGenerator = keyGenerator;
            this.worklog = worklog;
        }

        [HttpPut, Route("create")]
        public async Task<string> Create(CreateModel model)
        {
            var id = keyGenerator.LikeGuid();

            await messageBus.Publish(new CreateWorklogCommand(id, model.Description, model.Minutes, model.Date));

            return id;
        }

        [HttpGet, Route("get")]
        public Task<WorklogModel> Get(string id)
        {
            return worklog.GetById(id);
        }

        [HttpGet, Route("getall")]
        public Task<ICollection<WorklogModel>> GetAll()
        {
            return worklog.GetAll();
        }
    }
}