﻿using System.Configuration;
using Work;

namespace Web.Core.Configurations
{
    public class WorkSection : ConfigurationSection, IWorkConfiguration
    {
        private const string ConnectionStringKey = "connectionString";
        private const string DatabaseNameKey = "databaseName";

        public static IWorkConfiguration Configuration => ConfigurationManager.GetSection("workConfiguration") as IWorkConfiguration;

        [ConfigurationProperty(ConnectionStringKey, IsRequired = true)]
        public string ConnectionString => (string)base[ConnectionStringKey];

        [ConfigurationProperty(DatabaseNameKey, IsRequired = true)]
        public string DatabaseName => (string)base[DatabaseNameKey];
    }
}