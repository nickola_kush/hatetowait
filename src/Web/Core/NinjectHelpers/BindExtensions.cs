using System;
using System.Collections.Generic;
using System.Linq;
using Ninject.Syntax;

namespace Web.Core.NinjectHelpers
{
    public static class BindExtensions
    {
        public static void BindAllInterfaces(this IBindingRoot kernel, Type baseType, IEnumerable<Type> allTypes)
        {
            var classes = allTypes.Where(p => p.IsClass && !p.IsAbstract);
            foreach (var implementation in classes)
            {
                var allInterfaces = implementation.GetInterfaces();
                var interfaceTypes = baseType.IsGenericTypeDefinition
                    ? allInterfaces.Where(i => IsAssignableToGenericTypeDefinition(i, baseType))
                    : allInterfaces.Where(baseType.IsAssignableFrom);

                foreach (var interfaceType in interfaceTypes)
                {
                    kernel.Bind(interfaceType).To(implementation);
                }
            }
        }

        private static bool IsAssignableToGenericTypeDefinition(Type type, Type genericTypeDefinition)
        {
            while (type != null)
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == genericTypeDefinition)
                {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }
    }
}