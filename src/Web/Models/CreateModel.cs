﻿using System;

namespace Web.Models
{
    public class CreateModel
    {
        public string Description { get; set; }
        public int Minutes { get; set; }
        public DateTime Date { get; set; }
    }
}