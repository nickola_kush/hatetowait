﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core;
using MongoDB.Driver;
using Work.Contract.Models;

namespace Work.ReadModel.Implementation
{
    public class InvoiceDao : IInvoiceDao
    {
        private readonly IKeyGenerator keyGenerator;
        private readonly IMongoCollection<InvoiceViewModel> collection;

        public InvoiceDao(IKeyGenerator keyGenerator, IWorkConfiguration configuration)
        {
            this.keyGenerator = keyGenerator;
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.DatabaseName);
            collection = database.GetCollection<InvoiceViewModel>(nameof(InvoiceViewModel));
        }

        public async Task<InvoiceViewModel> GetByYearAndMonth(int year, int month)
        {
            var id = keyGenerator.YearMonth(year, month);
            using (var cursor = await collection.FindAsync(f => f.Id == id, new FindOptions<InvoiceViewModel> { Limit = 1 }))
            {
                return await cursor.FirstOrDefaultAsync();
            }
        }

        public async Task<ICollection<InvoiceViewModel>> GetAll()
        {
            var findOptions = new FindOptions<InvoiceViewModel> { Sort = Builders<InvoiceViewModel>.Sort.Descending(f => f.Year).Ascending(f => f.Month) };
            using (var cursor = await collection.FindAsync(f => true, findOptions))
            {
                return await cursor.ToListAsync();
            }
        }
    }
}