﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Work.Contract.Models;

namespace Work.ReadModel.Implementation
{
    public class WorklogDao : IWorklogDao
    {
        private readonly IMongoCollection<WorklogModel> collection;

        public WorklogDao(IWorkConfiguration configuration)
        {
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.DatabaseName);
            collection = database.GetCollection<WorklogModel>(nameof(WorklogModel));
        }

        public async Task<WorklogModel> GetById(string id)
        {
            using (var cursor = await collection.FindAsync(f => f.Id == id, new FindOptions<WorklogModel> { Limit = 1 }))
            {
                return await cursor.FirstOrDefaultAsync();
            }
        }

        public async Task<ICollection<WorklogModel>> GetAll()
        {
            var findOptions = new FindOptions<WorklogModel> { Sort = Builders<WorklogModel>.Sort.Descending(f => f.UtcDateTicks) };
            using (var cursor = await collection.FindAsync(f => true, findOptions))
            {
                return await cursor.ToListAsync();
            }
        }
    }
}