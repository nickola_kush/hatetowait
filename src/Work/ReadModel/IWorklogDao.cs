﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Work.Contract.Models;

namespace Work.ReadModel
{
    public interface IWorklogDao
    {
        Task<WorklogModel> GetById(string id);
        Task<ICollection<WorklogModel>> GetAll();
    }
}