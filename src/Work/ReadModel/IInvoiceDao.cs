﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Work.Contract.Models;

namespace Work.ReadModel
{
    public interface IInvoiceDao
    {
        Task<InvoiceViewModel> GetByYearAndMonth(int year, int month);
        Task<ICollection<InvoiceViewModel>> GetAll();
    }
}
