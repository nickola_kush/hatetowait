﻿namespace Work
{
    public interface IWorkConfiguration
    {
        string ConnectionString { get; }
        string DatabaseName { get; }
    }
}