﻿using System.Threading.Tasks;
using Core;
using Core.Messaging;
using MongoDB.Driver;
using Work.Contract.Events;
using Work.Contract.Models;

namespace Work.Handlers
{
    public class InvoiceGenerator
        : IHandler<WorklogCreated>
    {
        private readonly IKeyGenerator keyGenerator;
        private readonly IMongoCollection<InvoiceViewModel> collection;

        public InvoiceGenerator(IKeyGenerator keyGenerator, IWorkConfiguration configuration)
        {
            this.keyGenerator = keyGenerator;
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.DatabaseName);
            collection = database.GetCollection<InvoiceViewModel>(nameof(InvoiceViewModel));
        }

        public Task Handle(WorklogCreated message)
        {
            var date = message.Date;
            var id = keyGenerator.YearMonth(date.Year, date.Month);

            var worklog = new WorklogViewModel
            {
                Id = message.SourceId,
                Description = message.Description,
                TimeInMinutes = message.TimeInMinutes
            };

            return Update(id,
                Builders<InvoiceViewModel>.Update.SetOnInsert(f => f.Month, date.Month),
                Builders<InvoiceViewModel>.Update.SetOnInsert(f => f.Year, date.Year),
                Builders<InvoiceViewModel>.Update.Inc(f => f.TotalMinutesSpent, message.TimeInMinutes),
                Builders<InvoiceViewModel>.Update.Push(f => f.Worklogs, worklog)
            );
        }

        private Task Update(string id, params UpdateDefinition<InvoiceViewModel>[] updates)
        {
            return collection.FindOneAndUpdateAsync(
                Builders<InvoiceViewModel>.Filter.Eq(f => f.Id, id),
                Builders<InvoiceViewModel>.Update.Combine(updates),
                new FindOneAndUpdateOptions<InvoiceViewModel> { IsUpsert = true }
            );
        }
    }
}