﻿using System.Threading.Tasks;
using Core.Messaging;
using MongoDB.Driver;
using Work.Contract.Commands;
using Work.Contract.Events;
using Work.Contract.Models;

namespace Work.Handlers
{
    public class WorklogCommandHandler :
        IHandler<CreateWorklogCommand>
    {
        private readonly IMessageBus messageBus;
        private readonly IMongoCollection<WorklogModel> collection;

        public WorklogCommandHandler(IWorkConfiguration configuration, IMessageBus messageBus)
        {
            this.messageBus = messageBus;
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.DatabaseName);
            collection = database.GetCollection<WorklogModel>(nameof(WorklogModel));
        }

        public async Task Handle(CreateWorklogCommand message)
        {
            var model = new WorklogModel
            {
                Id = message.Id,
                Description = message.Description,
                TimeInMinutes = message.TimeInMinutes,
                UtcDate = message.Date,
                UtcDateTicks = message.Date.Ticks
            };
            await Save(model);
            await messageBus.Publish(new WorklogCreated(message.Id, message.Description, message.TimeInMinutes, message.Date));
        }

        private Task Save(WorklogModel entity)
        {
            return collection.FindOneAndReplaceAsync(
                Builders<WorklogModel>.Filter.Eq(x => x.Id, entity.Id), entity,
                new FindOneAndReplaceOptions<WorklogModel> { IsUpsert = true });
        }
    }
}
