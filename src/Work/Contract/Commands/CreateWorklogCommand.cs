﻿using System;
using Core.Messaging;

namespace Work.Contract.Commands
{
    public class CreateWorklogCommand : ICommand
    {
        public string Id { get; }
        public string Description { get; }
        public int TimeInMinutes { get; }
        public DateTime Date { get; }

        public CreateWorklogCommand(string id, string description, int timeInMinutes, DateTime date)
        {
            Id = id;
            Description = description;
            TimeInMinutes = timeInMinutes;
            Date = date;
        }
    }
}