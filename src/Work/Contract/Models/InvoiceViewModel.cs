﻿using System.Collections.Generic;
using System.Linq;

namespace Work.Contract.Models
{
    public class InvoiceViewModel
    {
        public string Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int TotalMinutesSpent { get; set; }

        public IEnumerable<WorklogViewModel> Worklogs { get; set; } = Enumerable.Empty<WorklogViewModel>();
    }
}