﻿namespace Work.Contract.Models
{
    public class WorklogViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public int TimeInMinutes { get; set; }
    }
}