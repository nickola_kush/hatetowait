﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Work.Contract.Models
{
    [BsonIgnoreExtraElements]
    public class WorklogModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public int TimeInMinutes { get; set; }
        public DateTime UtcDate { get; set; }
        public long UtcDateTicks { get; set; }

        public override string ToString()
        {
            return $"Description: {Description}";
        }
    }
}