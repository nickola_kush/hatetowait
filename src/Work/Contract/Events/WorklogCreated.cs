﻿using System;
using Core.Messaging;

namespace Work.Contract.Events
{
    public class WorklogCreated : IEvent
    {
        public string SourceId { get; set; }
        public string Description { get; set; }
        public int TimeInMinutes { get; set; }
        public DateTime Date { get; set; }

        public WorklogCreated(string id, string description, int timeInMinutes, DateTime date)
        {
            SourceId = id;
            Description = description;
            TimeInMinutes = timeInMinutes;
            Date = date;
        }
    }
}
